// https://stackoverflow.com/a/5306832
Array.prototype.move = function (oldIndex, newIndex) {
  if (newIndex >= this.length) {
    let k = newIndex - this.length + 1;
    while (k--) this.push(undefined);
  }
  this.splice(newIndex, 0, this.splice(oldIndex, 1)[0]);
  return this;
};

const fs = require("fs");

const values = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .map((l, i) => ({ val: parseInt(l), index: i }));

const len = values.length;

for (let i = 0; i < len; i += 1) {
  const oldIndex = values.findIndex((x) => x.index === i);
  const val = values[oldIndex].val % (len - 1);
  if (val === 0) continue;
  let newIndex = oldIndex + val;
  if (newIndex <= 0) newIndex = len - (Math.abs(newIndex) % len) - 1;
  if (newIndex >= len) newIndex = (newIndex % len) + 1;
  values.move(oldIndex, newIndex);
}

let index = values.findIndex((x) => x.val === 0);
let sum = 0;
const numbers = [];
// 1000th, 2000th, 3000th
for (const step of [1000, 1000, 1000]) {
  index = (index + step) % len;
  numbers.push(values[index].val);
  sum += values[index].val;
}
console.log(numbers, sum);
