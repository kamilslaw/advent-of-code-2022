const fs = require("fs");

const array = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((str) => str.split("").map((c) => parseInt(c)));

const X = array[0].length;
const Y = array.length;

let maxScore = 0;
for (let x = 0; x < X; x += 1) {
  for (let y = 0; y < Y; y += 1) {
    const score = getScore(x, y);
    if (score > maxScore) {
      maxScore = score;
    }
  }
}

function getScore(x, y) {
  if (x === 0 || y === 0 || x === X - 1 || y === Y - 1) {
    return 0;
  }
  const val = array[y][x];
  let l = 0,
    r = 0,
    u = 0,
    d = 0;
  for (let yy = y - 1; yy >= 0; yy -= 1) {
    u += 1;
    if (array[yy][x] >= val) break;
  }
  for (let yy = y + 1; yy < Y; yy += 1) {
    d += 1;
    if (array[yy][x] >= val) break;
  }
  for (let xx = x - 1; xx >= 0; xx -= 1) {
    l += 1;
    if (array[y][xx] >= val) break;
  }
  for (let xx = x + 1; xx < X; xx += 1) {
    r += 1;
    if (array[y][xx] >= val) break;
  }
  return l * r * u * d;
}

console.log(maxScore);
