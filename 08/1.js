const fs = require("fs");

const array = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((str) => str.split("").map((c) => parseInt(c)));

const X = array[0].length;
const Y = array.length;

let result = 0;
for (let x = 0; x < X; x += 1) {
  for (let y = 0; y < Y; y += 1) {
    if (!isHidden(x, y)) {
      result += 1;
    }
  }
}

function isHidden(x, y) {
  if (x === 0 || y === 0 || x === X - 1 || y === Y - 1) {
    return false;
  }
  const val = array[y][x];
  let sides = 0;
  for (let yy = 0; yy < y; yy += 1) {
    if (array[yy][x] >= val) {
      sides += 1;
      break;
    }
  }
  for (let yy = y + 1; yy < Y; yy += 1) {
    if (array[yy][x] >= val) {
      sides += 1;
      break;
    }
  }
  for (let xx = 0; xx < x; xx += 1) {
    if (array[y][xx] >= val) {
      sides += 1;
      break;
    }
  }
  for (let xx = x + 1; xx < X; xx += 1) {
    if (array[y][xx] >= val) {
      sides += 1;
      break;
    }
  }
  return sides === 4;
}

console.log(result);
