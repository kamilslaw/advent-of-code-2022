const obj = require("fs")
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .reduce((acc, x) => {
    const parts = x.split(": ");
    const key = parts[0];
    const eq = parts[1].split(" ");
    acc[key] =
      eq.length === 1
        ? {
            exec: () => (key === "humn" ? NaN : parseInt(eq[0])),
          }
        : {
            exec: () =>
              key === "root"
                ? eval(`${acc[eq[0]].exec()}===${acc[eq[2]].exec()}`)
                : eval(`${acc[eq[0]].exec()}${eq[1]}${acc[eq[2]].exec()}`),
            op: key === "root" ? "===" : eq[1],
            l: eq[0],
            r: eq[2],
          };
    return acc;
  }, {});

let pointer = "root";
let desiredValue = -1;
while (pointer !== "humn") {
  const op = obj[pointer].op;
  const ok = isNaN(obj[obj[pointer].l].exec())
    ? obj[pointer].r
    : obj[pointer].l;
  const notOk = ok === obj[pointer].l ? obj[pointer].r : obj[pointer].l;
  const okValue = obj[ok].exec();
  const isLeft = notOk === obj[pointer].l;
  desiredValue =
    op === "==="
      ? okValue
      : op === "+"
      ? desiredValue - okValue
      : op === "*"
      ? desiredValue / okValue
      : op === "-" && isLeft
      ? desiredValue + okValue
      : op === "-" && !isLeft
      ? okValue - desiredValue
      : op === "/" && isLeft
      ? desiredValue * okValue
      : op === "/" && isLeft
      ? okValue / desiredValue
      : -1;
  pointer = notOk;
}

console.log(desiredValue);
