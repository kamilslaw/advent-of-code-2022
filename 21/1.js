console.log(
  require("fs")
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((l) => !!l)
    .reduce((acc, x) => {
      const parts = x.split(": ");
      const eq = parts[1].split(" ");
      acc[parts[0]] =
        eq.length === 1
          ? () => parseInt(eq[0])
          : () => eval(`${acc[eq[0]]()}${eq[1]}${acc[eq[2]]()}`);
      return acc;
    }, {})
    .root()
);
