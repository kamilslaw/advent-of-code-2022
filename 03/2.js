const fs = require("fs");

Array.prototype.groupEveryN = function (n) {
  return this.reduce(
    (acc, x) => {
      const last = acc.at(-1);
      if (last.length === n) return acc.concat([[x]]);
      last.push(x);
      return acc;
    },
    [[]]
  );
};

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .groupEveryN(3)
  .map((group) => {
    const r1 = group[0].split("");
    const r2 = group[1].split("");
    const r3 = group[2].split("");
    const commonType = r1.find((c) => r2.includes(c) && r3.includes(c));
    const asciiValue = commonType.charCodeAt();
    return asciiValue > 96 ? asciiValue - 96 : asciiValue - 38;
  })
  .reduce((acc, x) => acc + x, 0);

console.log(result);
