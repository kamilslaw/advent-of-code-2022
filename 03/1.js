const fs = require("fs");

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const compartment1 = line.slice(0, line.length / 2).split("");
    const compartment2 = line.slice(line.length / 2).split("");
    const commonType = compartment1.find((c) => compartment2.includes(c));
    const asciiValue = commonType.charCodeAt();
    return asciiValue > 96 ? asciiValue - 96 : asciiValue - 38;
  })
  .reduce((acc, x) => acc + x, 0);

console.log(result);
