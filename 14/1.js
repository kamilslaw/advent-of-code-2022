const fs = require("fs");

const map = {};
const sandStartPos = { x: 500, y: 0 };
const voidThresholdY = 200;
const possibleMoves = [
  ({ y, x }) => ({ y: y + 1, x }),
  ({ y, x }) => ({ y: y + 1, x: x - 1 }),
  ({ y, x }) => ({ y: y + 1, x: x + 1 }),
];
let stillSandUnits = 0;
let currentSandPos = { ...sandStartPos };

fs.readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(" -> ").map((s) => s.split(",").map((x) => parseInt(x))))
  .forEach(fillWithRocks);

while (currentSandPos.y < voidThresholdY) {
  if (!updateSandPos()) {
    fill(currentSandPos);
    currentSandPos = { ...sandStartPos };
    stillSandUnits += 1;
  }
}

console.log(stillSandUnits);

function updateSandPos() {
  for (const possibleMove of possibleMoves) {
    const newPos = possibleMove(currentSandPos);
    if (!isOccupied(newPos)) {
      currentSandPos = newPos;
      return true;
    }
  }
  return false;
}

function isOccupied(pos) {
  return !!map[`${pos.x}|${pos.y}`];
}

function fill(pos) {
  map[`${pos.x}|${pos.y}`] = true;
}

function fillWithRocks(lines) {
  for (let i = 0; i < lines.length - 1; i += 1) {
    const x1 = lines[i][0];
    const y1 = lines[i][1];
    const x2 = lines[i + 1][0];
    const y2 = lines[i + 1][1];
    if (x1 === x2)
      for (let y = Math.min(y1, y2); y <= Math.max(y1, y2); y += 1)
        fill({ x: x1, y: y });
    else if (y1 === y2)
      for (let x = Math.min(x1, x2); x <= Math.max(x1, x2); x += 1)
        fill({ x, y: y1 });
    else throw new Error("not possible, diagonal line");
  }
}
