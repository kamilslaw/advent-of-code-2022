const fs = require("fs");

const strategies = {
  r: { win: "p", lose: "s" },
  p: { win: "s", lose: "r" },
  s: { win: "r", lose: "p" },
};

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const parts = line.split(" ");
    const op = parts[0] === "A" ? "r" : parts[0] === "B" ? "p" : "s";
    const me =
      parts[1] === "X"
        ? strategies[op].lose
        : parts[1] === "Z"
        ? strategies[op].win
        : op;
    const moveScore = me === "r" ? 1 : me === "p" ? 2 : 3;
    const roundScore =
      op === me
        ? 3
        : (me === "r" && op == "s") ||
          (me === "p" && op == "r") ||
          (me === "s" && op == "p")
        ? 6
        : 0;
    return roundScore + moveScore;
  })
  .reduce((acc, x) => acc + x, 0);

console.log(result);
