const fs = require("fs");

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const parts = line.split(" ");
    const op = parts[0] === "A" ? "r" : parts[0] === "B" ? "p" : "s";
    const me = parts[1] === "X" ? "r" : parts[1] === "Y" ? "p" : "s";
    const moveScore = me === "r" ? 1 : me === "p" ? 2 : 3;
    const roundScore =
      op === me
        ? 3
        : (me === "r" && op == "s") ||
          (me === "p" && op == "r") ||
          (me === "s" && op == "p")
        ? 6
        : 0;
    return roundScore + moveScore;
  })
  .reduce((acc, x) => acc + x, 0);

console.log(result);
