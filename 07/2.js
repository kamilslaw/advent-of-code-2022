const fs = require("fs");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l);

const root = { parent: undefined, files: {}, subdirs: {} };
let cursor = undefined;

for (const line of lines) {
  if (line === "$ cd /") {
    cursor = root;
  } else if (line === "$ cd ..") {
    cursor = cursor.parent;
  } else if (line.startsWith("$ cd")) {
    const subdir = line.split(" ")[2];
    if (!cursor.subdirs[subdir]) {
      cursor.subdirs[subdir] = { parent: cursor, files: {}, subdirs: {} };
    }
    cursor = cursor.subdirs[subdir];
  } else if (line === "$ ls") {
    // nothing
  } else if (line.startsWith("dir ")) {
    // nothing
  } else {
    const file = line.split(" ");
    cursor.files[file[1]] = parseInt(file[0]);
  }
}

const sizes = [];
function travel(dir) {
  const subdirsSize = Object.values(dir.subdirs)
    .map(travel)
    .reduce((acc, x) => acc + x, 0);
  const filesSize = Object.values(dir.files).reduce((acc, x) => acc + x, 0);
  const totalSize = subdirsSize + filesSize;
  sizes.push(totalSize);
  return totalSize;
}

const rootSize = travel(root);
const filesystemSize = 70_000_000;
const targetFreeSize = 30_000_000;
const currentFreeSize = filesystemSize - rootSize;
console.log(
  sizes.filter((s) => currentFreeSize + s >= targetFreeSize).sort((a, b) => a - b)[0]
);
