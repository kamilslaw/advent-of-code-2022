const fs = require("fs");

const NUMBER_OF_TAILS = 9;
const visited = new Set();
const hPos = { x: 0, y: 0 };
const tPos = [];
for (let i = 0; i < NUMBER_OF_TAILS; i += 1) tPos.push({ x: 0, y: 0 });

fs.readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .flatMap((l) => {
    const parts = l.split(" ");
    const dir = parts[0];
    const count = parseInt(parts[1]);
    return Array(count).fill(dir); // translates "D 3" into ["D", "D", "D"]
  })
  .forEach((m) => {
    if (m === "U") hPos.y += 1;
    else if (m === "D") hPos.y -= 1;
    else if (m === "R") hPos.x += 1;
    else if (m === "L") hPos.x -= 1;

    follow(tPos[0], hPos);
    for (let i = 1; i < NUMBER_OF_TAILS; i += 1) follow(tPos[i], tPos[i - 1]);

    visited.add(JSON.stringify(tPos.at(-1)));
  });

function follow(t, h) {
  if (Math.abs(t.x - h.x) > 1 || Math.abs(t.y - h.y) > 1) {
    if (h.x > t.x) t.x += 1;
    else if (h.x < t.x) t.x -= 1;
    if (h.y > t.y) t.y += 1;
    else if (h.y < t.y) t.y -= 1;
  }
}

console.log(visited.size);
