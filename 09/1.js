const fs = require("fs");

const visited = new Set();
const hPos = { x: 0, y: 0 };
const tPos = { x: 0, y: 0 };

fs.readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .flatMap((l) => {
    const parts = l.split(" ");
    const dir = parts[0];
    const count = parseInt(parts[1]);
    return Array(count).fill(dir); // translates "D 3" into ["D", "D", "D"]
  })
  .forEach((m) => {
    if (m === "U") hPos.y += 1;
    else if (m === "D") hPos.y -= 1;
    else if (m === "R") hPos.x += 1;
    else if (m === "L") hPos.x -= 1;

    if (Math.abs(hPos.x - tPos.x) <= 1 && Math.abs(hPos.y - tPos.y) <= 1) {
      // do nothing
    } else {
      // move
      if (hPos.x > tPos.x) tPos.x += 1;
      else if (hPos.x < tPos.x) tPos.x -= 1;
      if (hPos.y > tPos.y) tPos.y += 1;
      else if (hPos.y < tPos.y) tPos.y -= 1;
    }

    visited.add(JSON.stringify(tPos));
  });

console.log(visited.size);
