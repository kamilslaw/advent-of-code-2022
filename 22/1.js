const data = require("fs")
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n\r?\n/)
  .filter((l) => !!l);
let map = data[0].split(/\r?\n/);
const X = Math.max(...map.map((row) => row.length));
const Y = map.length;
map = map.map((row) => row + " ".repeat(X - row.length)); // make rectangle map by adding spaces at the end
const path = data[1];
const get = ({ x, y }) => map[y][x];
const moves = [
  ({ x, y }) => {
    const newPos = { x: (x + 1) % X, y };
    while (get(newPos) === " ") newPos.x = (newPos.x + 1) % X;
    if (get(newPos) === "#") return { x, y };
    return newPos;
  },
  ({ x, y }) => {
    const newPos = { x, y: (y + 1) % Y };
    while (get(newPos) === " ") newPos.y = (newPos.y + 1) % Y;
    if (get(newPos) === "#") return { x, y };
    return newPos;
  },
  ({ x, y }) => {
    const newPos = { x: x == 0 ? X - 1 : x - 1, y };
    while (get(newPos) === " ") newPos.x = newPos.x == 0 ? X - 1 : newPos.x - 1;
    if (get(newPos) === "#") return { x, y };
    return newPos;
  },
  ({ x, y }) => {
    const newPos = { x, y: y == 0 ? Y - 1 : y - 1 };
    while (get(newPos) === " ") newPos.y = newPos.y == 0 ? Y - 1 : newPos.y - 1;
    if (get(newPos) === "#") return { x, y };
    return newPos;
  },
];

let pos = { x: map[0].indexOf("."), y: 0 };
let dir = 0;
let curr = "";
for (let i = 0; i < path.length; i += 1) {
  if (path[i] === "L" || path[i] === "R") {
    for (let j = parseInt(curr); j > 0; j -= 1) pos = moves[dir](pos);
    curr = "";
    if (path[i] === "L") dir = dir == 0 ? moves.length - 1 : dir - 1;
    else dir = (dir + 1) % moves.length;
  } else curr += path[i];
}
for (let j = parseInt(curr); j > 0; j -= 1) pos = moves[dir](pos);

console.log(pos, 1000 * (pos.y + 1) + 4 * (pos.x + 1) + dir);
