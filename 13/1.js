const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n\r?\n/)
    .filter((l) => !!l)
    .map(parsePair)
    .map(isPairRight)
    .reduce((acc, res, i) => acc + (res >= 0 ? i + 1 : 0), 0)
);

function parsePair(str) {
  const lines = str.split(/\r?\n/);
  return { l: JSON.parse(lines[0]), r: JSON.parse(lines[1]) };
}

function isPairRight({ l, r }) {
  if (!Array.isArray(l)) l = [l];
  if (!Array.isArray(r)) r = [r];
  for (let i = 0; i < l.length; i += 1) {
    if (r[i] === undefined) return -1;
    if (Array.isArray(l[i]) || Array.isArray(r[i])) {
      const isInnerPairRight = isPairRight({ l: l[i], r: r[i] });
      if (isInnerPairRight !== 0) return isInnerPairRight;
    } else {
      if (l[i] < r[i]) return 1;
      if (l[i] > r[i]) return -1;
    }
  }
  if (r.length > l.length) return 1;
  return 0;
}
