const fs = require("fs");

const dividerPackets = [[[2]], [[6]]];

const sortedPackets = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(JSON.parse)
  .concat(dividerPackets)
  .sort((a, b) => (isPairRight(a, b) < 0 ? 1 : -1))
  .map(JSON.stringify);

console.log(
  dividerPackets
    .map(JSON.stringify)
    .map((s) => sortedPackets.indexOf(s))
    .reduce((acc, x) => acc * (x + 1), 1)
);

function isPairRight(l, r) {
  if (!Array.isArray(l)) l = [l];
  if (!Array.isArray(r)) r = [r];
  for (let i = 0; i < l.length; i += 1) {
    if (r[i] === undefined) return -1;
    if (Array.isArray(l[i]) || Array.isArray(r[i])) {
      const isInnerPairRight = isPairRight(l[i], r[i]);
      if (isInnerPairRight !== 0) return isInnerPairRight;
    } else {
      if (l[i] < r[i]) return 1;
      if (l[i] > r[i]) return -1;
    }
  }
  if (r.length > l.length) return 1;
  return 0;
}
