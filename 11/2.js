const fs = require("fs");

const cache = new Map();

const monkeys = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n\r?\n/)
  .filter((s) => !!s)
  .map((s) => {
    const lines = s.split(/\r?\n/);
    const args = lines[2].split("= old ")[1].split(" ");
    const operation = args[0] === "+" ? (a, b) => a + b : (a, b) => a * b;
    const secondArg = parseInt(args[1]);
    const divisibleBy = parseInt(lines[3].split("divisible by ")[1]);
    const ifTrue = parseInt(lines[4].split("monkey ")[1]);
    const ifFalse = parseInt(lines[5].split("monkey ")[1]);
    return {
      inspections: 0,
      items: lines[1]
        .split(": ")[1]
        .split(", ")
        .map((x) => parseInt(x)),
      transform: (a) => operation(a, isNaN(secondArg) ? a : secondArg),
      divisibleBy,
      getNewMonkey: (item) => (item % divisibleBy === 0 ? ifTrue : ifFalse),
    };
  });

const product = monkeys.reduce((acc, m) => acc * m.divisibleBy, 1);

let numberOfRounds = 10_000;
while (numberOfRounds--) {
  for (const monkey of monkeys) {
    while (monkey.items.length) {
      monkey.inspections += 1;
      const item = monkey.transform(monkey.items.shift()) % product;
      monkeys[monkey.getNewMonkey(item)].items.push(item);
    }
  }
}

console.log(monkeys.map((m) => m.inspections));
console.log(
  monkeys
    .map((m) => m.inspections)
    .sort((a, b) => b - a)
    .slice(0, 2)
    .reduce((acc, x) => acc * x, 1)
);
