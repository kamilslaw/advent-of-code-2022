const fs = require("fs");

const pattern = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/)[0];
const rocks = [
  "####\n    \n    \n    ",
  " #  \n### \n #  \n    ",
  "  # \n  # \n### \n    ",
  "#   \n#   \n#   \n#   ",
  "##  \n##  \n    \n    ",
].map((r) => r.split("\n"));
const rockSymbols = ["#", "@", "$", "&", "*"];
const rockHeights = [1, 3, 3, 4, 2];
const width = 7;
const startX = 2;
const world = {};
let stoppedRocks = 0;
let totalHeight = 0;
let patternIndex = 0;
let rockIndex = 0;
let rockX = null;
let rockY = null;
let patternMove = true;
let log = "";

addWallsToWorld();

while (stoppedRocks < 2022) makeMove();

for (let y = totalHeight + 5; y >= 0; y -= 1) {
  let line = y.toString().padStart(4, "0") + " | ";
  for (let x = -1; x <= width; x += 1) line += world[`${y}|${x}`] || " ";
  console.log(line);
}
console.log(new Date(), "Total Height:", totalHeight);
console.log(new Date(), "Total Rocks:", stoppedRocks);
fs.writeFileSync("log.txt", log);

function makeMove() {
  if (rockX === null) {
    rockX = startX;
    rockY = totalHeight + 3 + rockHeights[rockIndex];
    patternMove = true;
  }

  if (patternMove) {
    if (pattern[patternIndex] === ">" && canGoRight()) rockX += 1;
    if (pattern[patternIndex] === "<" && canGoLeft()) rockX -= 1;
    patternIndex = (patternIndex + 1) % pattern.length;
  } else {
    if (canGoDown()) {
      rockY -= 1;
    } else {
      addRockToWorld();
      stoppedRocks += 1;
      totalHeight = Math.max(rockY, totalHeight);
      log += `${stoppedRocks}:${totalHeight}\n`;
      rockX = null;
      rockIndex = (rockIndex + 1) % rocks.length;
    }
  }

  patternMove = !patternMove;
}

function addRockToWorld() {
  const rock = rocks[rockIndex];
  for (let y = 0; y < 4; y += 1)
    for (let x = 0; x < 4; x += 1)
      if (rock[y][x] === "#")
        world[`${rockY - y}|${rockX + x}`] = rockSymbols[rockIndex];
}

function addWallsToWorld() {
  for (let y = 0; y < 100_000; y += 1) {
    world[`${y}|-1`] = "|";
    world[`${y}|${width}`] = "|";
  }
  for (let x = 0; x < width; x += 1) world[`0|${x}`] = "-";
}

function canGoRight() {
  return canGo(
    (y) => y,
    (x) => x + 1
  );
}

function canGoLeft() {
  return canGo(
    (y) => y,
    (x) => x - 1
  );
}

function canGoDown() {
  return canGo(
    (y) => y - 1,
    (x) => x
  );
}

function canGo(transformY, transformX) {
  const rock = rocks[rockIndex];
  for (let y = 0; y < 4; y += 1)
    for (let x = 0; x < 4; x += 1)
      if (
        rock[y][x] === "#" &&
        !!world[`${transformY(rockY - y)}|${transformX(rockX + x)}`]
      )
        return false;
  return true;
}
