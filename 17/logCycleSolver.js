const fs = require("fs");

const log = [[-1, -1]].concat(
  fs
    .readFileSync("log.txt", "utf-8")
    .split(/\r?\n/)
    .filter((l) => !!l)
    .map((l) => l.split(":").map((x) => parseInt(x)))
);

const cycleMaxSize = 5_000;
const cycleMaxStart = 4_000;

for (let x = cycleMaxStart; x >= 1; x -= 1) {
  for (let y = cycleMaxSize; y >= 100; y -= 1) {
    const c1 = log[x + y][1];
    const c2 = log[x + y * 2][1];
    const c3 = log[x + y * 3][1];
    const c4 = log[x + y * 4][1];
    const c5 = log[x + y * 5][1];
    if (c3 - c2 === c2 - c1 && c3 - c2 === c5 - c4 && c3 - c2 === c4 - c3) {
      throw new Error(
        "FOUND IT! cycle starts at " +
          x +
          "th rock and it has " +
          y +
          " rocks in it | " +
          JSON.stringify([
            log[x + y],
            log[x + y * 2],
            log[x + y * 3],
            log[x + y * 4],
            log[x + y * 5],
          ])
      );
    }
  }
}

throw new Error("Could not find a cycle");
