const fs = require("fs");

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n\r?\n/)
  .map((group) =>
    group
      .split(/\r?\n/)
      .map((x) => parseInt(x))
      .reduce((acc, x) => acc + x, 0)
  )
  .sort((a, b) => b - a)
  .slice(0, 3)
  .reduce((acc, x) => acc + x, 0);

console.log(result);
