const fs = require("fs");

const input = fs.readFileSync("input.txt", "utf-8").split("");
const distinctSize = 4;

for (let i = distinctSize; i <= input.length; i += 1) {
  if (new Set(input.slice(i - distinctSize, i)).size == distinctSize) {
    console.log(i);
    break;
  }
}
