const fs = require("fs");

let calls = {};
const totalTimeout = 32;
const results = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .slice(0, 3)
  .map((l) => l.split(" "))
  .map((parts) => ({
    index: parseInt(parts[1].replace(":", "")),
    costs: {
      ore: parseInt(parts[6]),
      clay: parseInt(parts[12]),
      obsidian: {
        ore: parseInt(parts[18]),
        clay: parseInt(parts[21]),
      },
      geode: {
        ore: parseInt(parts[27]),
        obsidian: parseInt(parts[30]),
      },
    },
  }))
  .map((blueprint) => {
    console.log(new Date(), "Calculating for blueprint", blueprint.index);
    const initialState = {
      robots: { ore: 1, clay: 0, obsidian: 0, geode: 0 },
      resources: { ore: 0, clay: 0, obsidian: 0, geode: 0 },
      log: "",
    };
    calls[blueprint.index] = 0;
    const result = getMaxGeodesOpened(blueprint, initialState, totalTimeout);
    console.log(
      new Date(),
      "For blueprint",
      blueprint.index,
      "result is",
      result,
      "calls:",
      calls[blueprint.index]
    );
    return result;
  });

console.log(
  new Date(),
  "Results: ",
  results,
  results.reduce((acc, x) => acc * x, 1)
);

// Optimalizations
// 1) if Geode robot can be bought, don't check any other state
// 2) if Obsidian robot can be bought and there is no other Obsidian robot, don't check any other state
// 3) if new Geode robot can't be bought with remaining resources, robots and time, calculate remaining value
// 4) if Ore or Clay or Obsidian robots can be bought (2 of 3), don't check no buy state
// 5) if there is no Clay robot after 9 steps, STOP

function getMaxGeodesOpened(b, s, timeout) {
  calls[b.index] += 1;
  if (timeout < totalTimeout - 9 && s.robots.clay === 0) return 0;
  if (timeout === 0) return s.resources.geode;
  if (s.robots.geode > 0 && !newGeodeCanBeBought(b, s, timeout))
    return s.resources.geode + s.robots.geode * timeout;
  const newStates = [];

  if (canBuyGeode(s, b)) newStates.push(buyGeode(s, b));
  else {
    if (canBuyObsidian(s, b) && s.robots.obsidian === 0)
      newStates.push(buyObsidian(s, b));
    else {
      if (canBuyOre(s, b)) newStates.push(buyOre(s, b));
      if (canBuyClay(s, b)) newStates.push(buyClay(s, b));
      if (canBuyObsidian(s, b)) newStates.push(buyObsidian(s, b));
      if (newStates.length < 2) newStates.push(noBuy(s));
    }
  }

  return Math.max(
    ...newStates.map((s) => getMaxGeodesOpened(b, s, timeout - 1))
  );
}

function noBuy(s) {
  return {
    robots: { ...s.robots },
    resources: {
      ore: s.resources.ore + s.robots.ore,
      clay: s.resources.clay + s.robots.clay,
      obsidian: s.resources.obsidian + s.robots.obsidian,
      geode: s.resources.geode + s.robots.geode,
    },
    log: s.log + "noBuy;",
  };
}

function canBuyObsidian(s, b) {
  return (
    s.resources.ore >= b.costs.obsidian.ore &&
    s.resources.clay >= b.costs.obsidian.clay
  );
}

function buyObsidian(s, b) {
  return {
    robots: { ...s.robots, obsidian: s.robots.obsidian + 1 },
    resources: {
      ore: s.resources.ore + s.robots.ore - b.costs.obsidian.ore,
      clay: s.resources.clay + s.robots.clay - b.costs.obsidian.clay,
      obsidian: s.resources.obsidian + s.robots.obsidian,
      geode: s.resources.geode + s.robots.geode,
    },
    log: s.log + "ob;",
  };
}

function canBuyClay(s, b) {
  return s.resources.ore >= b.costs.clay;
}

function buyClay(s, b) {
  return {
    robots: { ...s.robots, clay: s.robots.clay + 1 },
    resources: {
      ore: s.resources.ore + s.robots.ore - b.costs.clay,
      clay: s.resources.clay + s.robots.clay,
      obsidian: s.resources.obsidian + s.robots.obsidian,
      geode: s.resources.geode + s.robots.geode,
    },
    log: s.log + "cl;",
  };
}

function canBuyOre(s, b) {
  return s.resources.ore >= b.costs.ore;
}

function buyOre(s, b) {
  return {
    robots: { ...s.robots, ore: s.robots.ore + 1 },
    resources: {
      ore: s.resources.ore + s.robots.ore - b.costs.ore,
      clay: s.resources.clay + s.robots.clay,
      obsidian: s.resources.obsidian + s.robots.obsidian,
      geode: s.resources.geode + s.robots.geode,
    },
    log: s.log + "or;",
  };
}

function canBuyGeode(s, b) {
  return (
    s.resources.ore >= b.costs.geode.ore &&
    s.resources.obsidian >= b.costs.geode.obsidian
  );
}

function buyGeode(s, b) {
  return {
    robots: { ...s.robots, geode: s.robots.geode + 1 },
    resources: {
      ore: s.resources.ore + s.robots.ore - b.costs.geode.ore,
      clay: s.resources.clay + s.robots.clay,
      obsidian:
        s.resources.obsidian + s.robots.obsidian - b.costs.geode.obsidian,
      geode: s.resources.geode + s.robots.geode,
    },
    log: s.log + "ge;",
  };
}

function newGeodeCanBeBought(b, s, timeout) {
  const ore = s.resources.ore + timeout * s.robots.ore;
  const obsidian = s.resources.obsidian + timeout * s.robots.obsidian;
  return ore >= b.costs.geode.ore && obsidian >= b.costs.geode.obsidian;
}
