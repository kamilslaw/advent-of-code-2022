const fs = require("fs");

const grid = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(""));
const Y = grid.length - 1;
const X = grid[0].length - 1;

const startY = grid.findIndex((l) => l.includes("S"));
const startPos = { y: startY, x: grid[startY].findIndex((c) => c === "S") };
const endY = grid.findIndex((l) => l.includes("E"));
const endPos = { y: endY, x: grid[endY].findIndex((c) => c === "E") };

const distances = grid.map((l) => l.map((_) => Number.MAX_SAFE_INTEGER));
setValue(startPos, 0);
const queue = grid.flatMap((a, y) => a.map((_, x) => ({ y, x })));

while (queue.length) {
  queue.sort((a, b) => getValue(b) - getValue(a));
  const pos = queue.pop();
  const value = getValue(pos);
  getNeighbors(pos)
    .filter((n) => canGo(pos, n) && getValue(n) > value + 1)
    .forEach((n) => setValue(n, value + 1));
}

console.log(getValue(endPos));

function canGo(from, to) {
  const normalize = (c) => (c === "S" ? "a" : c === "E" ? "z" : c);
  const a = normalize(getChar(from));
  const b = normalize(getChar(to));
  return b.charCodeAt(0) - a.charCodeAt(0) <= 1;
}

function getChar(pos) {
  return grid[pos.y][pos.x];
}

function getValue(pos) {
  return distances[pos.y][pos.x];
}

function setValue(pos, value) {
  distances[pos.y][pos.x] = value;
}

function getNeighbors(pos) {
  const result = [];
  if (pos.y > 0) result.push({ y: pos.y - 1, x: pos.x });
  if (pos.y < Y) result.push({ y: pos.y + 1, x: pos.x });
  if (pos.x > 0) result.push({ y: pos.y, x: pos.x - 1 });
  if (pos.x < X) result.push({ y: pos.y, x: pos.x + 1 });
  return result;
}
