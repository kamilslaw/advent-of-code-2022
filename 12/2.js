const fs = require("fs");

const grid = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split("").map((c) => (c === "S" ? "a" : c)));
const Y = grid.length - 1;
const X = grid[0].length - 1;

const endY = grid.findIndex((l) => l.includes("E"));
const endPos = { y: endY, x: grid[endY].findIndex((c) => c === "E") };

const startPositions = [];
for (let y = 0; y <= Y; y += 1)
  for (let x = 0; x <= X; x += 1)
    if (getChar({ y, x }) === "a") startPositions.push({ y, x });

const results = startPositions
  .filter((pos) => getNeighbors(pos).some((n) => getChar(n) === "b")) // ignore 'a' that can't go directly to 'b'
  .map(getValueFrom);
console.log(results);
console.log(results.sort()[0]);

function getValueFrom(startPos) {
  const distances = grid.map((l) => l.map((_) => Number.MAX_SAFE_INTEGER));
  setValue(distances, startPos, 0);
  const queue = grid.flatMap((a, y) => a.map((_, x) => ({ y, x })));

  while (queue.length) {
    queue.sort((a, b) => getValue(distances, b) - getValue(distances, a));
    const pos = queue.pop();
    const value = getValue(distances, pos);
    getNeighbors(pos)
      .filter((n) => canGo(pos, n) && getValue(distances, n) > value + 1)
      .forEach((n) => setValue(distances, n, value + 1));
  }

  return getValue(distances, endPos);
}

function canGo(from, to) {
  const normalize = (c) => (c === "E" ? "z" : c);
  const a = normalize(getChar(from));
  const b = normalize(getChar(to));
  return b.charCodeAt(0) - a.charCodeAt(0) <= 1;
}

function getChar(pos) {
  return grid[pos.y][pos.x];
}

function getValue(distances, pos) {
  return distances[pos.y][pos.x];
}

function setValue(distances, pos, value) {
  distances[pos.y][pos.x] = value;
}

function getNeighbors(pos) {
  const result = [];
  if (pos.y > 0) result.push({ y: pos.y - 1, x: pos.x });
  if (pos.y < Y) result.push({ y: pos.y + 1, x: pos.x });
  if (pos.x > 0) result.push({ y: pos.y, x: pos.x - 1 });
  if (pos.x < X) result.push({ y: pos.y, x: pos.x + 1 });
  return result;
}
