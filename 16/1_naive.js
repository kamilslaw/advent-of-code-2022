const fs = require("fs");

const data = fs
  .readFileSync("input2.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(parse)
  .reduce((acc, x) => ({ ...acc, [x.id]: x }), {});
const nonZeroCount = Object.values(data).filter((x) => x.rate > 0).length;

let calls = 0;
console.log(new Date(), "Result:         ", maximize(["AA"], "AA", {}, 30));
console.log(new Date(), "Number of steps:", calls);

function parse(line) {
  return {
    id: line.split(" ")[1],
    rate: parseInt(line.split(";")[0].split("=")[1]),
    to: line
      .split(" ")
      .slice(9)
      .map((s) => s.replace(",", "")),
  };
}

function maximize(noOpenPath, current, opened, left) {
  calls += 1;
  if (left === 0 || Object.keys(opened).length === nonZeroCount) return 0;
  const goS = data[current].to
    .filter((x) => !noOpenPath.includes(x))
    .map((x) => maximize(noOpenPath.concat([x]), x, opened, left - 1));
  if (!!opened[current] || data[current].rate === 0) {
    return Math.max(...goS);
  } else {
    opened = { [current]: true, ...opened };
    const openS = data[current].to
      .map((x) => maximize([current], x, opened, left - 2))
      .map((s) => data[current].rate * (left - 1) + s);
    return Math.max(...goS, ...openS);
  }
}
