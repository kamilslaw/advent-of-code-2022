const fs = require("fs");

const data = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(parse)
  .reduce((acc, x) => ({ ...acc, [x.id]: x }), {});
const keys = Object.keys(data);
const nonZeroIds = Object.values(data)
  .filter((x) => x.rate > 0)
  .map((x) => x.id);
const paths = createGraphPaths();

let calls = 0;
console.log(new Date(), "Result: ", maximize(nonZeroIds, "AA", "AA", 30 + 1));
console.log(new Date(), "Number of steps:", calls);

function parse(line) {
  return {
    id: line.split(" ")[1],
    rate: parseInt(line.split(";")[0].split("=")[1]),
    to: line
      .split(" ")
      .slice(9)
      .map((s) => s.replace(",", "")),
  };
}

function maximize(notOpened, current, target, left) {
  calls += 1;
  if (!left || !notOpened.length) return 0;
  if (current === target) {
    notOpened = notOpened.filter((x) => x !== current);
    const value = data[current].rate * (left - 1);
    return !notOpened.length
      ? value
      : Math.max(
          ...notOpened
            .map((x) => maximize(notOpened, current, x, left - 1))
            .map((s) => s + value)
        );
  } else {
    const { next, steps } = paths[`${current}-${target}`];
    if (steps > left - 2) return 0;
    return maximize(notOpened, next, target, left - 1);
  }
}

function createGraphPaths() {
  const paths = {};
  console.log(new Date(), "Generating paths...");
  for (const from of keys) {
    for (const to of keys) {
      if (from === to) continue;
      paths[`${from}-${to}`] = data[from].to.includes(to)
        ? { next: to, steps: 1 }
        : dijkstra(from, to);
    }
  }
  console.log(new Date(), "Paths generated");
  return paths;
}

function dijkstra(from, to) {
  const d = keys.reduce((acc, x) => ({ ...acc, [x]: 1_000_000 }), {});
  const prev = keys.reduce((acc, x) => ({ ...acc, [x]: undefined }), {});
  d[from] = 0;
  const q = [...keys];
  while (q.length) {
    q.sort((a, b) => d[b] - d[a]);
    const u = q.pop();
    data[u].to.forEach((v) => {
      if (d[v] > d[u] + 1) {
        d[v] = d[u] + 1;
        prev[v] = u;
      }
    });
  }
  const steps = d[to];
  while (prev[to] !== from) to = prev[to];
  return { next: to, steps };
}
