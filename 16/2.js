const fs = require("fs");

const data = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(parse)
  .reduce((acc, x) => ({ ...acc, [x.id]: x }), {});
const keys = Object.keys(data);
const nonZeroIds = Object.values(data)
  .filter((x) => x.rate > 0)
  .map((x) => x.id);
const paths = createGraphPaths();

let calls = 0;
const me = { current: "AA", target: "AA" };
const elephant = { ...me };
console.log(
  new Date(),
  "Result:         ",
  maximize(nonZeroIds, [], [me, elephant], 26 + 1)
);
console.log(new Date(), "Number of steps:", calls);

function parse(line) {
  return {
    id: line.split(" ")[1],
    rate: parseInt(line.split(";")[0].split("=")[1]),
    to: line
      .split(" ")
      .slice(9)
      .map((s) => s.replace(",", "")),
  };
}

function stayInPlace({ current }) {
  return { current, target: current };
}

function moveForward({ current, target }) {
  const { next } = paths[`${current}-${target}`];
  return { current: next, target };
}

function newTarget({ current }, target) {
  return { current, target };
}

function maximize(notOpened, busy, team, left) {
  calls += 1;
  if (calls % 100_000_000 === 0) console.log(new Date(), calls);
  if (!left) return 0;
  let value = 0;
  team.forEach(({ current, target }) => {
    if (current === target && notOpened.includes(current)) {
      notOpened = notOpened.filter((x) => x !== current);
      busy = busy.filter((x) => x !== current);
      value += data[current].rate * (left - 1);
    }
  });
  if (!notOpened.length) return value;
  const results = [];

  // no decision to make
  if (team.every((x) => x.current !== x.target)) {
    results.push(maximize(notOpened, busy, team.map(moveForward), left - 1));
  } else {
    // single decision to make
    const inProgress = team.find((x) => x.current !== x.target);
    if (inProgress) {
      const inTarget = team.find((x) => x.current === x.target);
      const no = notOpened.filter((x) => !busy.includes(x));
      if (no.length)
        no.map((x) =>
          maximize(
            notOpened,
            busy.concat([x]),
            [moveForward(inProgress), newTarget(inTarget, x)],
            left - 1
          )
        ).forEach((x) => results.push(x));
      else
        results.push(
          maximize(
            notOpened,
            busy,
            [moveForward(inProgress), stayInPlace(inTarget)],
            left - 1
          )
        );
    } else {
      // two decisions to make
      if (notOpened.length === 1) {
        results.push(
          maximize(
            notOpened,
            busy.concat([notOpened[0]]),
            team.map((x) => newTarget(x, notOpened[0])),
            left - 1
          )
        );
      } else {
        const no = notOpened;
        for (let i = 0; i < no.length - 1; i += 1)
          for (let j = i + 1; j < no.length; j += 1) {
            const dist0toI = paths[`${team[0].current}-${no[i]}`].steps;
            const dist0toJ = paths[`${team[0].current}-${no[j]}`].steps;
            const dist1toI = paths[`${team[1].current}-${no[i]}`].steps;
            const dist1toJ = paths[`${team[1].current}-${no[j]}`].steps;
            if (
              team[0].current === team[1].current ||
              (dist0toI < dist1toI && dist1toJ < dist0toJ)
            ) {
              results.push(
                maximize(
                  notOpened,
                  busy.concat([no[i], no[j]]),
                  [newTarget(team[0], no[i]), newTarget(team[1], no[j])],
                  left - 1
                )
              );
            } else if (dist0toJ < dist1toJ && dist1toI < dist0toI) {
              results.push(
                maximize(
                  notOpened,
                  busy.concat([no[i], no[j]]),
                  [newTarget(team[0], no[j]), newTarget(team[1], no[i])],
                  left - 1
                )
              );
            } else {
              results.push(
                maximize(
                  notOpened,
                  busy.concat([no[i], no[j]]),
                  [newTarget(team[0], no[i]), newTarget(team[1], no[j])],
                  left - 1
                )
              );
              results.push(
                maximize(
                  notOpened,
                  busy.concat([no[i], no[j]]),
                  [newTarget(team[0], no[j]), newTarget(team[1], no[i])],
                  left - 1
                )
              );
            }
          }
      }
    }
  }

  return value + Math.max(...results);
}

function createGraphPaths() {
  const paths = {};
  console.log(new Date(), "Generating paths...");
  for (const from of keys) {
    for (const to of keys) {
      if (from === to) continue;
      paths[`${from}-${to}`] = data[from].to.includes(to)
        ? { next: to, steps: 1 }
        : dijkstra(from, to);
    }
  }
  console.log(new Date(), "Paths generated");
  return paths;
}

function dijkstra(from, to) {
  const d = keys.reduce((acc, x) => ({ ...acc, [x]: 1_000_000 }), {});
  const prev = keys.reduce((acc, x) => ({ ...acc, [x]: undefined }), {});
  d[from] = 0;
  const q = [...keys];
  while (q.length) {
    q.sort((a, b) => d[b] - d[a]);
    const u = q.pop();
    data[u].to.forEach((v) => {
      if (d[v] > d[u] + 1) {
        d[v] = d[u] + 1;
        prev[v] = u;
      }
    });
  }
  const steps = d[to];
  while (prev[to] !== from) to = prev[to];
  return { next: to, steps };
}
