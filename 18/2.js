const fs = require("fs");

const input = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(",").map((x) => parseInt(x)));
const dimensions = [...Array(input[0].length).keys()];
const minDim = dimensions
  .map((d) => Math.min(...input.map((f) => f[d])))
  .map((x) => x - 1);
const maxDim = dimensions
  .map((d) => Math.max(...input.map((f) => f[d])))
  .map((x) => x + 1);
const figures = input.reduce((acc, x) => {
  acc[x.join()] = true;
  return acc;
}, {});
const visited = { [minDim.join()]: true };
const queue = [minDim];

let result = 0;
while (queue.length) {
  const pos = queue.pop();
  for (const dimension of dimensions)
    for (const transition of [-1, 1]) {
      const newPos = t(pos, dimension, transition);
      if (newPos.some((v, i) => v > maxDim[i] || v < minDim[i])) continue;
      if (figures[newPos.join()]) result += 1;
      else if (!visited[newPos.join()]) {
        visited[newPos.join()] = true;
        queue.push(newPos);
      }
    }
}

console.log(result);

function t(figure, dimension, value) {
  return figure.map((w, d) => (d === dimension ? w + value : w));
}
