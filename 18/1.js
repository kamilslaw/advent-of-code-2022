const fs = require("fs");

const figures = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(",").map((x) => parseInt(x)));
const dimensions = figures[0].length;

let result = 0;
for (const figure of figures)
  for (let d = 0; d < dimensions; d += 1)
    for (const transition of [-1, 1])
      if (!figures.some((f) => areEqual(t(figure, d, transition), f)))
        result += 1;

console.log(result);

function t(figure, dimension, value) {
  return figure.map((w, d) => (d === dimension ? w + value : w));
}

function areEqual(a, b) {
  return JSON.stringify(a) === JSON.stringify(b);
}
