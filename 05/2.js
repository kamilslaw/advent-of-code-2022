const fs = require("fs");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l);
const numberOfStacks = (lines[0].length + 1) / 4;
const biggestStackSize = lines.findIndex((l) => l.startsWith(" 1"));
const firstLineWithMove = biggestStackSize + 1;
const stacks = [];
for (let i = 0; i < numberOfStacks; i += 1) stacks.push([]);

for (let i = biggestStackSize - 1; i >= 0; i -= 1)
  for (let j = 0; j < numberOfStacks; j += 1) {
    const char = lines[i].charAt(j * 4 + 1);
    if (char != " ") stacks[j].push(char);
  }

for (let i = firstLineWithMove; i < lines.length; i += 1) {
  const parts = lines[i].split(" ");
  const count = parseInt(parts[1]);
  const from = parseInt(parts[3]) - 1;
  const to = parseInt(parts[5]) - 1;
  const tmpStack = [];
  for (let j = 0; j < count; j += 1) tmpStack.push(stacks[from].pop());
  while (tmpStack.length) stacks[to].push(tmpStack.pop());
}

const result = stacks.map((s) => s.at(-1)).join("");
console.log(result);
