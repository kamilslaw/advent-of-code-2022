const fs = require("fs");

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(",").map((s) => s.split("-").map((x) => parseInt(x))))
  .filter(
    (r) =>
      (r[0][0] <= r[1][0] && r[0][1] >= r[1][1]) ||
      (r[1][0] <= r[0][0] && r[1][1] >= r[0][1])
  ).length;

console.log(result);
