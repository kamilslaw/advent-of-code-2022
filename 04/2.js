const fs = require("fs");

const result = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map((l) => l.split(",").map((s) => s.split("-").map((x) => parseInt(x))))
  .map((r) => (r[0][0] < r[1][0] ? r : [r[1], r[0]]))
  .filter((r) => r[0][1] >= r[1][0]).length;

console.log(result);
