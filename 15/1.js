const fs = require("fs");

console.log(
  countFieldsWithNoBeacon(
    fs
      .readFileSync("input.txt", "utf-8")
      .split(/\r?\n/)
      .filter((l) => !!l)
      .map(parse),
    2_000_000
  )
);

function parse(line) {
  const sensorX = parseInt(line.split(",")[0].split("=")[1]);
  const sensorY = parseInt(line.split(":")[0].split("y=")[1]);
  const beaconX = parseInt(line.split(",")[1].split("x=")[1]);
  const beaconY = parseInt(line.split(",")[2].split("=")[1]);
  return {
    sensor: { x: sensorX, y: sensorY },
    beacon: { x: beaconX, y: beaconY },
    distance: Math.abs(sensorX - beaconX) + Math.abs(sensorY - beaconY),
  };
}

function toStr(pos) {
  return `${pos.x}|${pos.y}`;
}

// fields in row 'y' we can be sure there is no beacon
function countFieldsWithNoBeacon(data, y) {
  const beaconsPos = data.map((d) => toStr(d.beacon));
  const consideredFields = {};
  for (const pair of data) {
    const yDiff = Math.abs(y - pair.sensor.y);
    if (yDiff > pair.distance) continue;
    const startX = pair.sensor.x - pair.distance + yDiff;
    const endX = pair.sensor.x + pair.distance - yDiff;
    for (let x = startX; x <= endX; x += 1)
      if (!beaconsPos.includes(toStr({ x, y })))
        consideredFields[toStr({ x, y })] = true;
  }
  return Object.keys(consideredFields).length;
}
