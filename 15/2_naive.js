const fs = require("fs");

const minY = 0;
const minX = 0;
const maxY = 20;
const maxX = 20;

const data = fs
  .readFileSync("test_input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(parse);

const fields = getCheckedFields(data);

for (let x = minX; x <= maxX; x += 1)
  for (let y = minY; y <= maxY; y += 1)
    if (!fields[toStr({ x, y })]) {
      console.log({ x, y }, x * 4_000_000 + y);
      return;
    }

function parse(line) {
  const sensorX = parseInt(line.split(",")[0].split("=")[1]);
  const sensorY = parseInt(line.split(":")[0].split("y=")[1]);
  const beaconX = parseInt(line.split(",")[1].split("x=")[1]);
  const beaconY = parseInt(line.split(",")[2].split("=")[1]);
  return {
    sensor: { x: sensorX, y: sensorY },
    beacon: { x: beaconX, y: beaconY },
    distance: Math.abs(sensorX - beaconX) + Math.abs(sensorY - beaconY),
  };
}

function toStr(pos) {
  return `${pos.x}|${pos.y}`;
}

function getCheckedFields(data) {
  const fields = {};
  for (const pair of data) {
    console.log(new Date(), "Checking pair...", pair);
    const startY = pair.sensor.y - pair.distance;
    const endY = pair.sensor.y + pair.distance;
    for (let y = startY; y <= endY; y += 1) {
      const yDiff = Math.abs(y - pair.sensor.y);
      const startX = pair.sensor.x - pair.distance + yDiff;
      const endX = pair.sensor.x + pair.distance - yDiff;
      for (let x = startX; x <= endX; x += 1)
        if (x >= minX && x <= maxX && y >= minY && y <= maxY)
          fields[toStr({ x, y })] = true;
    }
  }
  return fields;
}
