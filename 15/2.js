const fs = require("fs");

const minY = 0;
const minX = 0;
const maxY = 4_000_000;
const maxX = 4_000_000;

const data = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .map(parse);

const pointsToCheck = getPointsToCheck(data);
console.log("Number of points to check: ", pointsToCheck.length);

for (const { x, y } of pointsToCheck) {
  let any = false;
  for (const pair of data) {
    const distance = Math.abs(x - pair.sensor.x) + Math.abs(y - pair.sensor.y);
    if (distance <= pair.distance) {
      any = true;
      break;
    }
  }
  if (!any) {
    console.log({ x, y }, x * 4_000_000 + y);
    return;
  }
}

function parse(line) {
  const sensorX = parseInt(line.split(",")[0].split("=")[1]);
  const sensorY = parseInt(line.split(":")[0].split("y=")[1]);
  const beaconX = parseInt(line.split(",")[1].split("x=")[1]);
  const beaconY = parseInt(line.split(",")[2].split("=")[1]);
  return {
    sensor: { x: sensorX, y: sensorY },
    distance: Math.abs(sensorX - beaconX) + Math.abs(sensorY - beaconY),
  };
}

function getPointsToCheck(data) {
  const fields = [];
  const pushIfValid = (pos) => {
    if (pos.x >= minX && pos.x <= maxX && pos.y >= minY && pos.y <= maxY)
      fields.push(pos);
  };
  for (const pair of data) {
    pushIfValid({ x: pair.sensor.x, y: pair.sensor.y - pair.distance - 1 }); // above
    pushIfValid({ x: pair.sensor.x, y: pair.sensor.y + pair.distance + 1 }); // below
    const startY = pair.sensor.y - pair.distance;
    const endY = pair.sensor.y + pair.distance;
    for (let y = startY; y <= endY; y += 1) {
      const yDiff = Math.abs(y - pair.sensor.y);
      const startX = pair.sensor.x - pair.distance + yDiff;
      const endX = pair.sensor.x + pair.distance - yDiff;
      pushIfValid({ x: startX - 1, y });
      pushIfValid({ x: endX + 1, y });
    }
  }
  return fields;
}
