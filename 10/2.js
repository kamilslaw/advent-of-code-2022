const fs = require("fs");

let x = 1;
const xValues = [NaN];

fs.readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .forEach((l) => {
    xValues.push(x);
    if (l !== "noop") {
      xValues.push(x);
      const inc = parseInt(l.split(" ")[1]);
      x += inc;
    }
  });

for (let i = 0; i < 6; i += 1) {
  let line = "";
  for (let j = 1; j <= 40; j += 1) {
    const char = Math.abs(xValues[j + i * 40] - j + 1) <= 1 ? "##" : "  ";
    line += char;
  }
  console.log(line);
  console.log(line);
}
