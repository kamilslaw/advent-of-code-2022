const fs = require("fs");

let x = 1;
const xValues = [NaN];
const getStrength = (cycle) => cycle * xValues[cycle];
const getStrengthSum = () =>
  getStrength(20) +
  getStrength(60) +
  getStrength(100) +
  getStrength(140) +
  getStrength(180) +
  getStrength(220);

fs.readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((l) => !!l)
  .forEach((l) => {
    xValues.push(x);
    if (l !== "noop") {
      xValues.push(x);
      const inc = parseInt(l.split(" ")[1]);
      x += inc;
    }
  });

console.log(getStrengthSum());
